# Contoso University sample app

* Contoso University demonstrates how to use Entity Framework Core in an ASP.NET Core MVC web application.

## Build it from scratch ##

* You can build the application by following the steps in a series of tutorials.

### Download it ###

* Download the completed project from GitHub by downloading or cloning the aspnet/Docs repository and navigating to aspnetcore\data\ef-mvc\intro\samples\cu-final in your local file system.

* After downloading the project, create the database by entering dotnet ef database update at a command-line prompt.

* As an alternative you can use Package Manager Console -- for more information, see Command-line interface (CLI) vs. Package Manager Console (PMC).